#include <stdio.h>
#include <math.h>

float get_radius()
{
    float radius;
    printf("enter radius\n");
    scanf("%f",&radius);
    return radius;
}
float compute_area(float radius)
{
    return 3.14*radius*radius;
}
float output(float radius,float area)
{
    printf("the area of the circle with radius=%f is %f\n",radius,area);
}

int main()
{
    float radius,area,circumference;
    radius = get_radius();
    area=compute_area(radius);

    output(radius,area);
    
    return 0;
}

/*
output_circumference(radius,circumference);
void output_circumference(float radius,float circumference)
{
    printf("the circumference of the circle with radius %f is %f\n",radius,circumference);
}*/